## Día 3

Se determinó lo que irá en la documentación, cómo debería seguirse el protocolo, incluyendo plan de contingencia, puesta en valor y mejoras en la infraestructura.

- Documentación
    - ¿Qué se hizo?
    - Herramientas: ¿qué son?, ¿como se utilizan?, ¿how to?
    - Todo al drive

- Protocolo
    ¿Cómo implementar todo lo que se aprendió?
    ¿Quienes van a gestionar las passwd?
    - LLave GPG en keepass
    - Información crítica
    - Disco externo cifrado
    - 2factor para todo -> mail - redes sociales (tomar captura de pantalla de QR por eventualidades -se pierde el tlf-)
    - Rotación de passwd - keepassxc - no storear la clave principal
        - Rotacion periodica + cerrar sesiones -> 1año con proyección a 6 meses
        - Rotacion adhoc cuando alguien deja la org + cambiar pwd keepass
        - Responsables de mantener actualizada la db y avisar de los cambios
        - Drive para db + key por signal + auto destrucción
    - FireFox + addons -> ublock origins - https everywhere - privacy badger - descentraleyes
    - Duckduckgo
    - GPG + firmado de llaves
    - Signal como IM
    - Encriptado de teléfonos - claves
    - Exif - borrado de metadata de aplicaciones
    - Tor
        - No cambiar el tamaño de la ventana + cosas
    - Migrar a Linux + cifrado de discos -> LUKS
    - Jitsi para video conferencias
    
    * Contingencia
        - Eventualidades
            - Resetear las passwd + cerrado de sesiones
            - Expulsar a la persona del grupo (en caso de que sea que una persona que se fue de la org) - celular
            - Bloquear al usuario en cuestión
            - Comunicar para que todos estén al tanto y continuar con el protocolo
        - Bitácora
            - Llamadas extrañas
            - Alguien los sigue
            - Prevenir
        - Basarse en la no confianza
            - Asegurarse que la persona del otro lado es la persona que dice ser
            
            
- Puesta en valor
    ¿Cómo transmitir el conocimiento obtenido?
    ¿como pasarlo a los demás?, eventos, escalar, talleres, otros
    - Documentación -> Protocolo -> Manual
    - Nuevos miembros
        - Introducción -> sombras
    - Miemrbos existentes
        - Reuniones
        - Talleres
        - Jornadas
    - Responsables - evangelistas
    - Usar agendas - planificación
    - Presupuesto -> recursos
    - Retrospectiva
    - Capacitación

- Mejoras de la infra
    Proyecciones:
    - Seguridad mail pasar a gmail
    - Backup a drive y a disco cifrado
    - UPS y estabilizadores
    - Wifi -> TP-Link isolar las redes de los demas, una red por oficina -> Project Lede como firmware -> Modo bridge
        - Vlans en switch caro, o varios chiquitos conectados en el router, para administrar las empresas.
    - Wordpress: mantenerlo actualizado
    - Seguridad de gDrive -> 2fa + keepass
    - VPN -> NordVpn - PIA
    - Servidores on premise (propios)
    - Whonix <-> Tor Box
    
Mail que redirecciona a gmail (cifrado por tls) pero bluehost no se sabe si está trifado
Averiguar si la configuración de email utiliza tls o algún cifrado
Se puede indicar una GPG para la recepción de mails (se puede guardar en keepass)

Definir como se guardará la base de datos del keepass, en la nube, localmente.
La contraseña del keepass no debe ir guardado en ningún lado, lo ideal es que todos los miembros lo memoricen.

- Backups
    No hay backups

- Herramientas
    - ADWCleaner
    - MalwareBytes
    - CCleaner
    - JRTool
